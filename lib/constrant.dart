import 'package:flutter/material.dart';

const ColorYellow = Color(0xFFFFC20D);
const ColorDarkYellow = Color(0xFFF2B501);
const ColorOrange = Color(0xFFEF6C00);

const ColorDark = Color(0xFF212121);
const ColorDarkGrey = Color(0xFF2C2C2C);
const ColorLightGrey = Color(0xFF272727);
const Blackcolor = Color(0xFF0D0D0D);

const ColorWhite = Color(0xFFF3F3F3);
const ColorWhiteGrey = Color(0xFFC6C5C5);
const ColorGrey = Color(0xFF8E8E8E);
const ColorGreySmall = Color(0xFFEEEEEE);

const ColorRed = Color(0xFFFF3D3D);
const ColorDarkRed = Color(0xFF8D2929);
