import 'package:flutter/material.dart';
import 'package:food_menu_app_ui/Home.dart';
import 'package:food_menu_app_ui/Navbottombar.dart';
import 'package:food_menu_app_ui/constrant.dart';

class OnBoardScreen extends StatefulWidget {
  OnBoardScreen();

  @override
  _OnBoardScreenState createState() => _OnBoardScreenState();
}

class _OnBoardScreenState extends State<OnBoardScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorOrange,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Stack(
            children: [
              Align(
                  alignment: Alignment.topRight,
                  child: Image.asset("./assets/images/rec.png")),
              Align(
                  alignment: Alignment.center,
                  child: Image.asset(
                    "./assets/images/masterchef.png",
                    height: 400.00,
                  )),
            ],
          ),
          Container(
            height: 300.00,
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(left: 32.00, right: 32.00, bottom: 32.00),
            padding: EdgeInsets.symmetric(vertical: 38.00),
            decoration: BoxDecoration(
              color: ColorDark,
              borderRadius: BorderRadius.circular(50.00),
              boxShadow: [
                BoxShadow(offset: Offset(0, 8), blurRadius: 6.00),
              ],
            ),
            child: Column(
              children: [
                SizedBox(height: 10.00),
                Text(
                  "How to cooking\nEasy to cook",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 22.00,
                    color: ColorWhite,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                SizedBox(
                  height: 22.0,
                ),
                Text(
                  "มีเมนูอาหารให้เลือกมากมาย\nในแอปเดียว",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 22.00,
                    color: ColorGrey,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(height: 23.0),
                GestureDetector(
                  onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => Navbottombar(index: 0),
                    ),
                  ),
                  child: Container(
                    height: 52.00,
                    width: 200.00,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: ColorYellow,
                      borderRadius: BorderRadius.circular(16.00),
                    ),
                    child: Text(
                      "Let's do it",
                      style: TextStyle(
                          fontSize: 22.00, fontWeight: FontWeight.w800),
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
