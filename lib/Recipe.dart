import 'package:flutter/material.dart';
import 'package:food_menu_app_ui/constrant.dart';
import 'package:like_button/like_button.dart';

class RecipeScreen extends StatefulWidget {
  final int index;
  final String image, name, time, level;
  String recipe;

  RecipeScreen(
      this.index, this.image, this.name, this.time, this.level, this.recipe);
  @override
  _RecipeScreenState createState() => _RecipeScreenState();
}

class _RecipeScreenState extends State<RecipeScreen> {
  int selectedIndex = 0;
  List tab = ["วิธีทำ", "ส่วนผสม"];
  List ingredientList = [
    {
      "image": "./assets/images/pasta.png",
      "name": "เส้นPasta",
    },
    {"image": "./assets/images/pork.png", "name": "หมูสับ/หมู"},
    {"image": "./assets/images/onion.png", "name": "หัวหอมใหญ่"},
    {"image": "./assets/images/salt.png", "name": "เกลือ"},
    {"image": "./assets/images/tomato.png", "name": "มะเขือเทศ"},
    {"image": "./assets/images/tomatosauce.png", "name": "ซอสมะเขือเทศ"},
  ];

  @override
  Widget build(BuildContext context) {
    List tabs = ["วิธีทำ", "ส่วนผสม"];
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(
                  top: 30.00, bottom: 0.00, left: 32.00, right: 32.00),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () => Navigator.of(context).pop(),
                    child: Container(
                      padding: EdgeInsets.all(10.00),
                      decoration: BoxDecoration(
                        color: Theme.of(context).accentColor,
                        borderRadius: BorderRadius.circular(14.00),
                      ),
                      child: Icon(
                        Icons.arrow_back_ios,
                        size: 24.00,
                        color: ColorGrey,
                      ),
                    ),
                  ),
                  GestureDetector(
                    child: Container(
                      padding: EdgeInsets.all(10.00),
                      decoration: BoxDecoration(
                        color: Theme.of(context).accentColor,
                        borderRadius: BorderRadius.circular(14.00),
                      ),
                      child: LikeButton(),
                    ),
                  ),
                ],
              ),
            ),
            Stack(
              overflow: Overflow.visible,
              children: [
                Hero(
                  tag: "tag${widget.image}",
                  child: Image.asset(
                    widget.image,
                    height: 300.00,
                    width: 400.00,
                    fit: BoxFit.contain,
                  ),
                ),
                Positioned(
                  top: 50.0,
                  right: 30.0,
                  child: Image.asset(
                    "./assets/icons/star.png",
                    height: 50.00,
                    fit: BoxFit.contain,
                  ),
                ),
                Positioned(
                  bottom: 50.0,
                  right: 320.0,
                  child: Image.asset(
                    "./assets/icons/star.png",
                    height: 50.00,
                    fit: BoxFit.contain,
                  ),
                ),
              ],
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(top: 28.00),
              padding: EdgeInsets.symmetric(vertical: 28.00),
              decoration: BoxDecoration(
                color: Theme.of(context).cardColor,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(50.00),
                  topRight: Radius.circular(50.00),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Theme.of(context).accentColor,
                  ),
                ],
              ),
              child: Column(
                children: [
                  Container(
                    height: 4.00,
                    width: 28.00,
                    margin: EdgeInsets.only(bottom: 32.00),
                    decoration: BoxDecoration(
                        color: ColorGrey,
                        borderRadius: BorderRadius.circular(20.00)),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      widget.name,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 28.00,
                        color: Theme.of(context).primaryColor,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  SizedBox(height: 25.00),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        children: [
                          Image.asset(
                            "./assets/icons/clockRecipe.png",
                            height: 30.00,
                            width: 30.00,
                            fit: BoxFit.contain,
                          ),
                          SizedBox(height: 10.00),
                          Text(
                            "${widget.time} Minute",
                            style: TextStyle(
                              fontSize: 16.00,
                              color: Theme.of(context).primaryColor,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(height: 4.00),
                          Text(
                            "Cooking",
                            style: TextStyle(
                              fontSize: 14.00,
                              color: Theme.of(context).primaryColor,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: List.generate(
                          6,
                          (index) => Container(
                            height: 2.00,
                            width: 2.00,
                            margin: EdgeInsets.only(bottom: 2.00),
                            decoration: BoxDecoration(
                              color: Theme.of(context).primaryColor,
                              borderRadius: BorderRadius.circular(2.00),
                            ),
                          ),
                        ),
                      ),
                      Column(
                        children: [
                          Image.asset(
                            "./assets/icons/star.png",
                            height: 30.00,
                            width: 30.00,
                            fit: BoxFit.contain,
                          ),
                          SizedBox(height: 10.00),
                          Text(
                            "5.00",
                            style: TextStyle(
                              fontSize: 16.00,
                              color: Theme.of(context).primaryColor,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(height: 4.00),
                          Text(
                            "Rating",
                            style: TextStyle(
                              fontSize: 14.00,
                              color: Theme.of(context).primaryColor,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: List.generate(
                          6,
                          (index) => Container(
                            height: 2.00,
                            width: 2.00,
                            margin: EdgeInsets.only(bottom: 2.00),
                            decoration: BoxDecoration(
                              color: Theme.of(context).primaryColor,
                              borderRadius: BorderRadius.circular(2.00),
                            ),
                          ),
                        ),
                      ),
                      Column(
                        children: [
                          Image.asset(
                            "./assets/icons/fire.png",
                            height: 30.00,
                            width: 30.00,
                            fit: BoxFit.contain,
                          ),
                          SizedBox(height: 10.00),
                          Text(
                            "${widget.level} Level",
                            style: TextStyle(
                              fontSize: 16.00,
                              color: Theme.of(context).primaryColor,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(height: 4.00),
                          Text(
                            "Recipe",
                            style: TextStyle(
                              fontSize: 14.00,
                              color: Theme.of(context).primaryColor,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 32.00,
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 30.00),
                    padding: EdgeInsets.symmetric(vertical: 6.00),
                    decoration: BoxDecoration(
                      color: Theme.of(context).cardColor,
                      borderRadius: BorderRadius.circular(36.00),
                      boxShadow: [
                        BoxShadow(
                          color: Theme.of(context).accentColor.withOpacity(1),
                        ),
                      ],
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: List.generate(
                        tabs.length,
                        (index) => GestureDetector(
                          onTap: () => setState(() {
                            selectedIndex = index;
                          }),
                          child: Container(
                            height: 48.00,
                            width: 160.00,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              color: selectedIndex == index
                                  ? Theme.of(context).dividerColor
                                  : Colors.transparent,
                              borderRadius: BorderRadius.circular(28.00),
                            ),
                            child: Text(
                              tabs[index],
                              style: TextStyle(
                                  fontSize: 16.00,
                                  fontWeight: FontWeight.w600,
                                  color: selectedIndex == index
                                      ? Theme.of(context).focusColor
                                      : ColorGrey),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 25.0),
                  selectedIndex == 0
                      ? Container(
                          height: 300.00,
                          margin: EdgeInsets.symmetric(horizontal: 16.00),
                          padding: EdgeInsets.symmetric(
                              horizontal: 16.00, vertical: 16.00),
                          decoration: BoxDecoration(
                            color: Theme.of(context).dividerColor,
                            borderRadius: BorderRadius.circular(24.00),
                          ),
                          child: Text(
                            widget.recipe,
                            style: TextStyle(
                                fontSize: 15.00,
                                color: Theme.of(context).primaryColor,
                                fontWeight: FontWeight.w600),
                          ),
                        )
                      : Container(
                          height: 130.00,
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: ingredientList.length,
                            itemBuilder: (context, index) {
                              return Container(
                                child: Container(
                                  height: 150.00,
                                  width: 140.00,
                                  margin: EdgeInsets.only(right: 10.00),
                                  decoration: BoxDecoration(
                                    color: Theme.of(context).dividerColor,
                                    borderRadius: BorderRadius.circular(24.00),
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                        padding: EdgeInsets.only(
                                            top: 20.00,
                                            bottom: 10.00,
                                            left: 0.00,
                                            right: 0.00),
                                        width: 50.00,
                                        child: Image.asset(
                                          ingredientList[index]["image"],
                                        ),
                                      ),
                                      SizedBox(height: 12.00),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 16.00),
                                        child: Text(
                                          ingredientList[index]["name"],
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            fontSize: 15.00,
                                            color:
                                                Theme.of(context).primaryColor,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                  SizedBox(height: 24.00),
                  GestureDetector(
                    onTap: () => Navigator.of(context).pop(),
                    child: Column(
                      children: [
                        Icon(
                          Icons.keyboard_arrow_up,
                          size: 28.00,
                          color: Theme.of(context).primaryColor,
                        ),
                        Text(
                          "Show more",
                          style: TextStyle(
                            fontSize: 16.00,
                            color: Theme.of(context).primaryColor,
                            fontWeight: FontWeight.w800,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
