import 'package:flutter/material.dart';
import 'package:food_menu_app_ui/List.dart';
import 'package:food_menu_app_ui/Recipe.dart';
import 'package:food_menu_app_ui/Search.dart';
import 'package:food_menu_app_ui/Setting.dart';
import 'package:food_menu_app_ui/constrant.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen();

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int selectedIndex = 0;
  int currentIndex = 0;
  List categoryList = [
    {
      "image": "./assets/images/spaghetti.png",
      "name": "เมนูเส้น",
    },
    {
      "image": "./assets/images/pancake.png",
      "name": "ขนมหวาน",
    },
    {
      "image": "./assets/images/rice.png",
      "name": "ข้าว",
    },
    {
      "image": "./assets/images/boiling.png",
      "name": "ต้ม",
    },
  ];
  List dinnerList = [
    {
      "image": "./assets/images/spagetti.png",
      "name": "สปาร์เก็ตตี้",
      "time": "50",
      "level": "Medium",
      "recipe":
          "1.ตั้งน้ำเปล่า ใส่เกลือและน้ำมันพืช พอน้ำเดือดใส่เส้นสปาเกตตีลงไปต้มจนสุกตามชอบ จัดใส่ภาชนะ \n2.ตั้งกระทะใส่เนยจืด ตามด้วยมะเขือเทศ และหอมใหญ่ ผัดคลุกเคล้าให้เข้ากัน \n3. ใส่หมูสับ เติมน้ำซุป และใบกระวานเพิ่มความหอม เคี่ยวจนหอมใหญ่และมะเขือเทศเริ่มเละ ปรุงรสด้วยซอสมะเขือเทศ เกลือ พริกไทย น้ำตาลทราย และผงปรุงรส เคี่ยวต่อไปเรื่อย ๆ จนน้ำข้น โรยออริกาโน่ลงไป \n4. ตักน้ำซอสราดบนเส้นสปาเกตตี โรยออริกาโน่เพิ่มอีกหน่อย หรือราดซอสมะเขือเทศ"
    },
    {
      "image": "./assets/images/meluearng.png",
      "name": "บะหมี่เหลือง",
      "time": "70",
      "level": "Hard",
      "recipe":
          "1. ตอกไข่ไก่ใส่อ่างผสม ใส่เกลือป่นลงไป ตามด้วยน้ำปูนใส ตีส่วนผสมให้เข้ากัน เตรียมไว้ \n2. ผสมแป้งขนมปัง แป้งสาลีอเนกประสงค์ และแป้งมันสำปะหลังเข้าด้วยกัน ใส่ลงในเครื่องตีแป้ง ใส่ส่วนผสมไข่ไก่ที่ตีไว้ลงไป เปิดเครื่องตีจนส่วนผสมแป้งไม่ติดก้นอ่าง \n3. แบ่งแป้งที่ตีแล้วเป็นก้อน ก้อนละเท่า ๆ กัน พักแป้งทิ้งไว้ประมาณ 20 นาที \n4. นำแป้งที่พักไว้จนครบเวลาแล้วมากดลงไปเบา ๆ จากนั้นโรยด้วยแป้งสาลีอเนกประสงค์ให้ทั่วทั้ง 2 ด้าน 9 วิธีทำเมนูบะหมี่ \n5. เตรียมเครื่องรีดแป้งให้พร้อม โดยปรับไปที่เลข 7 (โดยจะรีดขนาดหนาก่อน) นำแป้งใส่เครื่องรีดแป้งแล้วรีดแป้งเป็นแผ่น ๆ จากนั้นนำแผ่นแป้งออกจากเครื่อง โรยด้วยแป้งสาลีอเนกประสงค์ให้ทั่วทั้ง 2 ด้าน 9 วิธีทำเมนูบะหมี่ \n6. ปรับเครื่องรีดแป้งไปที่เลข 5 นำแผ่นแป้งที่รีดไว้ใส่ลงไปรีดอีกครั้ง (รีดให้บางลงเรื่อย ๆ) จากนั้นนำแผ่นแป้งออกจากเครื่อง โรยด้วยแป้งสาลีอเนกประสงค์ให้ทั่วทั้ง 2 ด้านอีกครั้ง 9 วิธีทำเมนูบะหมี่ \n7. สุดท้ายปรับเครื่องรีดแป้งไปที่เลข 1 แล้วนำแผ่นแป้งที่รีดไว้ใส่ลงไปรีดอีกครั้ง (รีดให้บางลงเรื่อย ๆ) จากนั้นนำออกจากเครื่อง โรยด้วยแป้งสาลีอเนกประสงค์ให้ทั่วทั้ง 2 ด้าน 9 วิธีทำเมนูบะหมี่ \n8. ในกรณีจะทำบะหมี่เส้นใหญ่ พอได้ความบางของแผ่นแป้งที่ต้องการแล้ว นำไปตัด-ซอยในเครื่องที่ช่องเส้นใหญ่ได้เลย (ตามภาพ) 9 วิธีทำเมนูบะหมี่ 9 วิธีทำเมนูบะหมี่ 9. ถ้าต้องการทำบะหมี่เส้นเล็กให้นำแป้งมาผึ่งแขวนก่อน เพื่อให้แป้งคลายตัวไม่หด และรอให้แผ่นแป้งแห้งลงหน่อย แต่ไม่ถึงกับแห้งแข็ง เพราะถ้าไม่ผึ่งให้แห้งพอประมาณ การซอยเป็นบะหมี่เส้นเล็กจะตัดไม่ขาดและจะติดออกมาเป็นแพ 9 วิธีทำเมนูบะหมี่ 10. นำแผ่นแป้งใส่เครื่องรีดแป้ง เลือกระดับเส้นจากเครื่องแล้วรีดแป้งออกมาเป็นเส้นตามต้องการ"
    },
    {
      "image": "./assets/images/guyjub.png",
      "name": "ก๊วยจั๊บ",
      "time": "45",
      "level": "Medium",
      "recipe":
          "1. ต้มน้ำสำหรับจะใช้ต้มเส้นก๋วยจั๋บ จะใส่ลงไปต้มทีละชาม หรือต้มไว้ทีเดียวเยอะๆเลยก็ได้ (แต่ต้มไว้เยอะๆ เส้นจะเละหน่อย) \n2. เครื่องในต่างๆ ทำคตวามสะอาดแล้วต้มให้สุก หั่นเป็นชิ้นๆ ไว้ \n3. ทำน้ำซุปก๋วยจั๊บน้ำซุปกระดูกหมูยกขึ้นตั้งไฟ พอเริ่มเดือด ใส่กระเทียมบุบ รากผักชีบุบ อบเชย โป้ยกั๊ก ผงพะโล้และเครื่องปรุงต่าง ๆ \n4. ค่อยๆ คนให้น้ำตาลละลาย ระวังอย่าให้ไหม้ติดก้นหม้อ ใส่เนื้อหมูหั่นชิ้นลงไป บางสูตร จะใช้การผัดในกระทะเครื่องต่างในกระทะกับเนื้อหมูแล้วค่อยถ่ายหรือเติมน้ำซุปลงไป \n5.ต้มจนเปื่อยดี จากนั้นตักเส้นก๋วยจั๊บใส่ชาม ตามด้วยเครื่องต่างๆ ตักน้ำซุปก๋วยจั๊บพร้อมด้วยเนื้อหมูที่ต้มจนเปื่อย โรยหน้าด้วย ต้นหอม กระเทียมเจียวและพริกไทยตามชอบ"
    },
    {
      "image": "./assets/images/mama.png",
      "name": "มาม่าต้มยำ",
      "time": "30",
      "level": "Easy",
      "recipe":
          "1. หมักเนื้อด้วยน้ำมันหอย/ซีอิ๊วขาว/น้ำตาล/ซีอิ๊วดำ/แป้งมันคลุกเคล้าให้เข้ากัน แช่ตู้เย็นอย่างน้อย30 นาทีหรือทิ้งไว้ทั้งคืน \n2. เจียวกระเทียมให้หอมใส่เนื้อที่หมักลงไปผัดใส่พริกไทยใส่หอมใหญ่และปรุงรสตามชอบ ผัดให้เข้ากันพักไว้ \n3. ต้มน้ำให้เดือดใส่เครื่องปรุงของมาม่าลงไป ปรุงรสด้วยน้ำตาลและน้ำส้มสายชู ซีอิ๊วขาวเล็กน้อย ตามด้วยเส้นมาม่า ต้มจนเส้นนุ่มได้ที่ \n4.เทใส่ชาม ตักเนื้อวางลงบนเส้น ใส่ไข่แดงโรยหน้าด้วยต้นหอมและพริก แค่นี้ก็เสริฟได้เลย"
    },
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(
                  top: 25.00, bottom: 20.00, left: 20.0, right: 20.00),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Welcome",
                              style: TextStyle(
                                fontSize: 28.00,
                                fontWeight: FontWeight.w800,
                                color: Theme.of(context).primaryColor,
                              ),
                            ),
                            Text(
                              "พร้อมสำหรับอาหารของคุณแล้วรึยัง ?",
                              style: TextStyle(
                                fontSize: 14.00,
                                fontWeight: FontWeight.w800,
                                color: Theme.of(context).primaryColor,
                              ),
                            ),
                          ],
                        ),
                      ),
                      GestureDetector(
                        onTap: () => Navigator.of(context).pop(),
                        child: Container(
                          decoration: BoxDecoration(
                            color: ColorOrange,
                            borderRadius: BorderRadius.circular(16.00),
                          ),
                          padding: EdgeInsets.only(
                            left: 6.0,
                            bottom: 6.0,
                            right: 6.0,
                            top: 6.0,
                          ),
                          margin: EdgeInsets.only(
                            left: 20.00,
                            bottom: 10.00,
                          ),
                          child: Image.asset(
                            "./assets/images/diet.png",
                            width: 50.00,
                          ),
                        ),
                      )
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 5),
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: 6.0,
                        color: ColorLightGrey,
                      ),
                      borderRadius: BorderRadius.circular(26.00),
                    ),
                    child: Stack(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(19.00),
                          child: Image.asset(
                            "./assets/images/foodbg.jpg",
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                            vertical: 24.0,
                            horizontal: 24.00,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Menu For You",
                                style: TextStyle(
                                  fontSize: 14.00,
                                  color: ColorWhiteGrey,
                                  fontWeight: FontWeight.w800,
                                ),
                              ),
                              SizedBox(height: 14.00),
                              Text(
                                "Chicken Bake",
                                style: TextStyle(
                                  fontSize: 22.00,
                                  color: ColorYellow,
                                  fontWeight: FontWeight.w800,
                                ),
                              ),
                              SizedBox(height: 12.00),
                              Row(
                                children: [
                                  Row(
                                    children: [
                                      Container(
                                        padding: EdgeInsets.all(2.0),
                                        margin: EdgeInsets.only(right: 8.00),
                                        decoration: BoxDecoration(
                                          color: ColorYellow,
                                          borderRadius:
                                              BorderRadius.circular(6.0),
                                        ),
                                        child: Image.asset(
                                          "./assets/icons/clock.png",
                                          width: 16.00,
                                          height: 16.00,
                                          fit: BoxFit.contain,
                                        ),
                                      ),
                                      Text(
                                        "30 min",
                                        style: TextStyle(
                                          fontSize: 14.00,
                                          color: ColorYellow,
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(width: 16.0),
                                  Row(
                                    children: [
                                      Container(
                                        padding: EdgeInsets.all(2.0),
                                        margin: EdgeInsets.only(
                                            right: 8.00, left: 8.0),
                                        decoration: BoxDecoration(
                                            color: ColorYellow,
                                            borderRadius:
                                                BorderRadius.circular(6.0)),
                                        child: Image.asset(
                                          "./assets/icons/cooking.png",
                                          width: 16.00,
                                          height: 16.00,
                                          fit: BoxFit.contain,
                                        ),
                                      ),
                                      Text(
                                        "Easy",
                                        style: TextStyle(
                                          fontSize: 14.00,
                                          color: ColorYellow,
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 8.00,
                      right: 8.00,
                      top: 8.0,
                      bottom: 8.00,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Category",
                          style: TextStyle(
                            fontSize: 22.0,
                            color: Theme.of(context).primaryColor,
                            fontWeight: FontWeight.w800,
                          ),
                        ),
                        Text(
                          "See All",
                          style: TextStyle(
                            fontSize: 14.0,
                            color: Theme.of(context).primaryColor,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 2.00),
                      child: Row(
                        children: List.generate(
                          categoryList.length,
                          (index) => Padding(
                            padding: const EdgeInsets.only(
                              right: 20.00,
                            ),
                            child: GestureDetector(
                              onTap: () => setState(() {
                                currentIndex = index;
                              }),
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                  vertical: 16.00,
                                ),
                                child: Container(
                                  padding: EdgeInsets.symmetric(
                                    vertical: 8.00,
                                    horizontal: 8.0,
                                  ),
                                  decoration: BoxDecoration(
                                    color: currentIndex == index
                                        ? Theme.of(context).buttonColor
                                        : Theme.of(context).cardColor,
                                    borderRadius: BorderRadius.circular(18.00),
                                    border: Border.all(
                                      width: 4.00,
                                      color: currentIndex == index
                                          ? Theme.of(context).buttonColor
                                          : Theme.of(context).accentColor,
                                    ),
                                  ),
                                  child: Row(
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(right: 16.00),
                                        padding: EdgeInsets.all(4.00),
                                        decoration: BoxDecoration(
                                          color: currentIndex == index
                                              ? Theme.of(context).buttonColor
                                              : Theme.of(context).cardColor,
                                          borderRadius:
                                              BorderRadius.circular(8.00),
                                        ),
                                        child: Image.asset(
                                          categoryList[index]["image"],
                                          height: 25.00,
                                          width: 25.00,
                                          fit: BoxFit.contain,
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(right: 16.0),
                                        child: Text(
                                          categoryList[index]["name"],
                                          style: TextStyle(
                                            fontSize: 18.00,
                                            fontWeight: FontWeight.w800,
                                            color: currentIndex == index
                                                ? Theme.of(context).hoverColor
                                                : Theme.of(context)
                                                    .primaryColor,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 16.00,
                      right: 16.00,
                      bottom: 16.00,
                    ),
                    child: GridView.builder(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        childAspectRatio: 1.00,
                        mainAxisSpacing: 75.00,
                        crossAxisSpacing: 0.00,
                      ),
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      padding: EdgeInsets.only(top: 50.00),
                      itemCount: dinnerList.length,
                      itemBuilder: (context, index) {
                        return GestureDetector(
                          onTap: () => Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (context) => RecipeScreen(
                                index,
                                dinnerList[index]["image"],
                                dinnerList[index]["name"],
                                dinnerList[index]["time"],
                                dinnerList[index]["level"],
                                dinnerList[index]["recipe"],
                              ),
                            ),
                          ),
                          child: Stack(
                            overflow: Overflow.visible,
                            alignment: Alignment.center,
                            children: [
                              Container(
                                height: 250.00,
                                width: 150.00,
                                padding: EdgeInsets.only(bottom: 10.00),
                                decoration: BoxDecoration(
                                  color: Theme.of(context).cardColor,
                                  borderRadius: BorderRadius.circular(32.00),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Theme.of(context)
                                          .accentColor
                                          .withOpacity(1),
                                    ),
                                  ],
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 12.00),
                                      child: Text(
                                        dinnerList[index]["name"],
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          fontSize: 16.00,
                                          fontWeight: FontWeight.w600,
                                          color: Theme.of(context).primaryColor,
                                        ),
                                      ),
                                    ),
                                    SizedBox(height: 12.00),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: List.generate(
                                        5,
                                        (index) => Image.asset(
                                          "./assets/icons/star.png",
                                          width: 20.00,
                                          height: 16.00,
                                        ),
                                      ),
                                    ),
                                    SizedBox(height: 12.00),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Text(
                                          "${dinnerList[index]["time"]}\nMin",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            fontSize: 12.00,
                                            fontWeight: FontWeight.w600,
                                            color:
                                                Theme.of(context).primaryColor,
                                          ),
                                        ),
                                        Column(
                                          children: List.generate(
                                            6,
                                            (index) => Container(
                                              height: 2.00,
                                              width: 2.00,
                                              margin:
                                                  EdgeInsets.only(bottom: 2.00),
                                              decoration: BoxDecoration(
                                                color: ColorGrey,
                                                borderRadius:
                                                    BorderRadius.circular(2.00),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Text(
                                          dinnerList[index]["level"],
                                          style: TextStyle(
                                            fontSize: 12.00,
                                            fontWeight: FontWeight.w600,
                                            color:
                                                Theme.of(context).primaryColor,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Positioned(
                                top: -170.00,
                                bottom: 0.00,
                                left: 0.00,
                                right: 0.00,
                                child: Hero(
                                  tag: "tag$index",
                                  child: Image.asset(
                                    dinnerList[index]["image"],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
