import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:food_menu_app_ui/constrant.dart';

class ThemeProvider extends ChangeNotifier {
  ThemeMode themeMode = ThemeMode.system;

  bool get isDarkMode {
    if (themeMode == ThemeMode.system) {
      final brightness = SchedulerBinding.instance?.window.platformBrightness;
      return brightness == Brightness.dark;
    } else {
      return themeMode == ThemeMode.dark;
    }
  }

  void toggleTheme(bool isOn) {
    themeMode = isOn ? ThemeMode.dark : ThemeMode.light;
    notifyListeners();
  }
}

class MyThemes {
  static final darkTheme = ThemeData(
    scaffoldBackgroundColor: ColorLightGrey,
    primaryColor: ColorWhiteGrey,
    buttonColor: ColorYellow,
    accentColor: ColorDarkGrey,
    dividerColor:ColorDarkGrey,
    focusColor:ColorDarkYellow,
    colorScheme: ColorScheme.dark(),
    cardColor: ColorDark,
    hoverColor: Colors.black,
    iconTheme: IconThemeData(color: Colors.white, opacity: 0.8),
  );

  static final lightTheme = ThemeData(
    scaffoldBackgroundColor: ColorGreySmall,
    primaryColor: ColorDark,
    buttonColor: ColorWhiteGrey,
    accentColor: ColorWhiteGrey,
    dividerColor:ColorWhiteGrey,
    focusColor:ColorDark,
    colorScheme: ColorScheme.light(),
    cardColor: ColorWhite,
    hoverColor: Colors.black,
    iconTheme: IconThemeData(color: Colors.black, opacity: 0.8),
  );
}
